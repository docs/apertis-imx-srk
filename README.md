# Apertis Habv4 keys && tooling

This repository contains the apertis super root keys used for development. The
keys were generated using the
[u-boot Hab v4 guide](https://gitlab.apertis.org/third-party/u-boot/blob/master/doc/imx/habv4/introduction_habv4.txt)

To program these keys on your device please see section 1.5 of the
[mx6/mx7 secure boot
guide](https://gitlab.apertis.org/third-party/u-boot/blob/master/doc/imx/habv4/guides/mx6_mx7_secure_boot.txt).
**This is a one-time unrecoverable only action**; Please be aware that once these keys are
fused on an MX6 device, they cannot be changed anymore. Only use for devices
that are only intended for Apertis development.

# HAB Booting on i.MX6 Sabre Automotive

A HAB boot chain starts with the bootloader; In the case of u-boot for sabre
automotive an SPL (setting up system basics, DDR RAM etc) followed by a full
u-boot image. The MX6 ROM will validate the SPL, the SPL needs to validate the
full u-boot image.

Unfortunately upstream u-boot currently doesn't do this correctly. An u-boot
with initial patches to solve this can be in the
[apertis u-boot repository](https://gitlab.apertis.org/third-party/u-boot/commits/sabreai-secure-boot-0)

## Build u-boot

To build this for sabre automotive the `mx6sabreauto_defconfig` should be
compiled which will output `SPL` and `u-boot-dtb.img` binaries. Both need to be
signed.

## signing SPL

The SPL can be signed using standard u-boot methods described in the 
[u-boot spl guide](https://gitlab.apertis.org/third-party/u-boot/blob/master/doc/imx/habv4/guides/mx6_mx7_spl_secure_boot.txt)
Section 1.3 and 1.4. And example CSF file is available as `csf/csf_spl.txt` in
this repository.

## signing u-boot-dtb.img

For sabre AI the u-boot image is actually a multi FIT image such that one
binary can be booted properly on all Sabre AI CPU boards. To sign this the
`csf/mangle-fit-image.py` tool in this repository should be used. The tool has
to be run in the `csf` directory  with the build `u-boot-dtb.img` as first
argument.

The tool requires the libfdt python bindings to installed on the systems and
the csf tool available in `$PATH`

## Flash SPL and u-boot

To boot standard Apertis images both binaries should be flashed into SPI flash
of the sabreauto. SPL is expected to be written into SPI flash at offset
`0x400` while the u-boot-dtb.img should be put at offset `0x20000`

To verify both SPL and u-boot were correctly flashed and signed boot the device
from SPI flash and run the `hab_status` command. This should indicate 
`No HAB Events Found!`.
