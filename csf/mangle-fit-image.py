#!/usr/bin/python3

import sys
import libfdt
import subprocess

from jinja2 import Environment, FileSystemLoader


LOADADDR = 0x17800000
IVT_LOADADDR = LOADADDR - 0x4000
FIT_LOADADDR= IVT_LOADADDR - 0x1000

e = Environment (loader=FileSystemLoader(''))

image = sys.argv[1]

f = open(image, 'r+b');
fdt = libfdt.FdtRo(f.read())

def image_info(i):
    n = fdt.path_offset("/images/" + i)
    offset = fdt.getprop(n, "data-offset").as_uint32()
    size = fdt.getprop(n, "data-size").as_uint32()
    return (fdt.totalsize() + offset, size)

def image_to_blocks (cnode, name, address):
    (offset, size) = image_info(fdt.getprop(cnode, name).as_str())
    return { 'address': address,
             'offset': offset,
             'size': size,
             'area': name,
             'name': image }

def insert_ivt (offset, address):
    f.seek(offset)

    f.write(b'\xD1') # IVT
    f.write(b'\x00\x20') # 0x20 bytes long
    f.write(b'\x40') # Hab v4

    f.write(FIT_LOADADDR.to_bytes(4, byteorder='little'))
    f.write((0).to_bytes(4, byteorder='little')) # reserved
    f.write((0).to_bytes(4, byteorder='little')) # DCD)
    f.write((0).to_bytes(4, byteorder='little')) #boot data
    f.write((address).to_bytes(4, byteorder='little')) # self pointer
    f.write((address + 0x20).to_bytes(4, byteorder='little')) # csf pointe
    f.write((0).to_bytes(4, byteorder='little')) # reserved 2
    f.flush()


def handle_configuration(cnode):
    blocks = []

    fit = { 'address': FIT_LOADADDR,
            'offset': 0,
            'size': fdt.totalsize(),
            'area': 'fit',
            'name': image }
    blocks.append(fit)

    firmware = image_to_blocks(cnode, "firmware", LOADADDR)
    blocks.append(firmware)

    blocks.append(image_to_blocks(cnode, "fdt",
                                   LOADADDR + firmware['size']))

    i = image_to_blocks(cnode, "ivtcsf", IVT_LOADADDR)
    i['size'] = 0x20
    blocks.append(i)

    insert_ivt(i['offset'], i['address'])

    outfile = "csf_uboot.%s" % fdt.get_name(cnode)
    out = open(outfile, "w")
    tmpl = e.get_template('csf_uboot.template')
    out.write(tmpl.render(blocks=blocks))
    out.close()

    # Signing
    signfile = "%s.bin" % outfile
    subprocess.run (["cst", "-i", outfile, "-o", signfile ], check=True)

    # Inject csf ; assume f is at the end of ivt
    c = open(signfile, "rb");
    f.write(c.read())
    c.close()

configurations = fdt.path_offset("/configurations")
cnode = fdt.first_subnode(configurations)
while True:
    handle_configuration(cnode)
    try:
        cnode = fdt.next_subnode(cnode)
    except Exception:
        break

